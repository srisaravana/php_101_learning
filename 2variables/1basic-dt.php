<?php


/*
 * string
 * int
 * float
 *
 * */

/*
 *
 * variable names -> identifier
 * values -> literals
 * */

$name = "Kumar";    // string
$age = 12;          // int
$pi = 3.1234;       // float

var_dump($name);
var_dump($age);
var_dump($pi);

$name = "David";

var_dump($name);


$color = "Blue";

var_dump($color);
