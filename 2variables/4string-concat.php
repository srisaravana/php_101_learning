<?php

$word1 = "hello";
$word2 = "world";

?>


<h1><?php echo "this is some text"; ?></h1>
<h1><?php echo 12; ?></h1>
<h1><?php echo 3.123456789; ?></h1>
<h1><?php echo 0; ?></h1>
<h1><?php echo false; ?></h1>
<h1><?php echo true; ?></h1>

<hr>

<h1><?php echo "this is long-hand method"; ?></h1>
<h1><?= "this is short-hand method"; ?></h1>

<hr>

<h2>
    <?php
    $a = 10;
    echo "this is some value";
    ?>
</h2>

<h2>
    <?=
    $a = 10;
    echo "this is some value";
    ?>
</h2>
