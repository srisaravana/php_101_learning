<?php
$word1 = "hello";
$word2 = "world";

$firstName = "Kumar";
$lastName = "Sanga";

//$fullName = $firstName . " " . $lastName;
$fullName = "$firstName $lastName";
//$fullName = '$firstName $lastName';

//$fullName = sprintf("%s", $firstName);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h3>My name is <?= $fullName; ?></h3>
<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi enim esse ex nam odit perferendis quae quibusdam quidem tempora temporibus. Aliquam
    doloremque fugiat minus molestiae necessitatibus nobis praesentium tenetur, vero!</p>

<p>
    Thanks,<br>
    <?= $fullName; ?>
</p>

</body>
</html>
