<?php

$name = "Kumar";
$age = 12;
$color = "red";

//$sentence = "My name is " . $name . " and I am " . $age . " years old. I like " . $color . " color";
$sentence = sprintf("My name is %s and I am %d years old. I like %s color.", $name, $age, $color);

/*
 * %s -> string
 * %d -> int
 * $f -> float
 * */

//$text1 = sprintf("%s %s", "world", "hello");
$text1 = sprintf("my age is %d", 12 + 56);
//$text1 = sprintf("pi = %.2f", 3.1415296);

?>

<h2>My name is Kumar and I am 12 years old and I like red color.</h2>

<h2><?= $sentence; ?></h2>

<h2><?php echo $text1; ?></h2>

<h2><?php echo sprintf("pi = %f", 3.1415296); ?></h2>
<h2><?php echo sprintf("pi = %.1f", 3.1415296); ?></h2>
<h2><?php echo sprintf("pi = %.2f", 3.1415296); ?></h2>
<h2><?php echo sprintf("pi = %.3f", 3.1415296); ?></h2>
<h2><?php echo sprintf("pi = %.4f", 3.1415296); ?></h2>
<h2><?php echo sprintf("pi = %.10f", 3.1415296124545121); ?></h2>
