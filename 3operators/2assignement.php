<?php

/*
 * assignment op (=)
 * */

$value1 = 3 + 5;
$value2 = 3 + 5 - 2;

$value3 = 4 + 2 * 3;
$value4 = 4 * 2 + 3;
$value5 = 4 * (2 + 3);


/*
 * assignment with incremental values
 * */

$age = 10;
// $age = $age + 6;
$age += 6; // $age = $age + 6;

$val = 10;
$val -= 4; // 6
$val *= 2; // 12
$val /= 3; // 4

?>

<h2><?= $value1; ?></h2>
<h2><?= $value2; ?></h2>

<h2><?= $value3; ?></h2>
<h2><?= $value4; ?></h2>
<h2><?= $value5; ?></h2>

<h2><?= $age; ?></h2>


