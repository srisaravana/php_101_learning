<?php

$val = 8;

$val++; // val += 1 // 9

var_dump($val); // 9

++$val; // val += 1 // 10

var_dump($val); // 10

// val = 10

//var_dump($val++);
//var_dump($val);

$number = 4 + $val++; // 4 + 10; val += 1
var_dump($number);
var_dump($val);

$number = 30 - ++$val; // val += 1; 30 - 10
var_dump($number);
var_dump($val);


