<?php

var_dump(4 < 3); // false
var_dump(3 < 4); // true
var_dump(4 > 3); // true
var_dump(3 > 4); // false
var_dump(3 >= 4); // false
var_dump(3 >= 3); // true
var_dump(3 >= 2); // true
var_dump(3 == 2); // false
var_dump(2 == 2); // true
var_dump(2 != 2); // false
var_dump(2 != 3); // true
var_dump(2 <> 3); // true



var_dump("2" == 2);
var_dump("2" !== 2);
