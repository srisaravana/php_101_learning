<?php

var_dump(true && true);
var_dump(3 > 2 && 4 != 4);
var_dump(true && true && true && false);

var_dump(true || true); // true
var_dump(true || false); // true
var_dump(false || false); // false

var_dump(!true);
var_dump(!false);
var_dump(!!false);
