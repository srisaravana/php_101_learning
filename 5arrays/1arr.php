<?php


$colors = []; // empty list

var_dump($colors);

$colors = ["red", "green", "blue"];

var_dump($colors);

?>

    <h3><?= $colors[0]; ?></h3>
    <h3><?= $colors[1]; ?></h3>
    <h3><?= $colors[2]; ?></h3>

<?php

$colors[0] = "pink";
var_dump($colors);

/* append new items at the end of the list */
$colors[] = "black";
$colors[] = "yellow";

var_dump($colors);

/* old method: using array func */
array_push($colors, "magenta");
var_dump($colors);

$colors[10] = "tomato";
var_dump($colors);
