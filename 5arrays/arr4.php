<?php


$book1 = [
    "title" => "The Lord of The Ring",
    "author" => "J.R.R Tolkien",
    "genre" => ["Classical Fantasy", "Fantasy", "Magical"],
    "price" => 23.99
];

$book2 = [
    "title" => "Sapiens",
    "author" => "Yoveal Hararri",
    "genre" => ["Science", "Anthropology"],
    "price" => 10.99
];

$book3 = [
    "title" => "Harry Potter",
    "author" => "J.K Rowling",
    "genre" => ["Kids Fantasy", "Magical"],
    "price" => 10.99
];

//$books = [$book1, $book2, $book3];
$books = [
    [
        "title" => "The Lord of The Ring",
        "author" => "J.R.R Tolkien",
        "genre" => ["Classical Fantasy", "Fantasy", "Magical"],
        "price" => 23.99
    ],
    [
        "title" => "Sapiens",
        "author" => "Yoveal Hararri",
        "genre" => ["Science", "Anthropology"],
        "price" => 10.99
    ],
    [
        "title" => "Harry Potter",
        "author" => "J.K Rowling",
        "genre" => ["Kids Fantasy", "Magical"],
        "price" => 15.99
    ],
];

var_dump($books);
