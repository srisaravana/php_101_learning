<?php

$age = 20;

if ($age >= 18) {
    echo "you are older than 18 years old <br>";
    echo "Thank you. <br>";
}

?>
<hr>
<?php

$var = 20;
// to use single line, we can skip brackets
if ($var > 10) echo "I like var <br>";

?>
<hr>

<?php

$gameOver = true; // boolean value

if ($gameOver) {
    echo "Game is over <br>";
} else {
    echo "Game is still on <br>";
}

?>
<hr>
<?php

$age = 20;

if ($age >= 18)
    echo "You are older than 18 years old. <br>";
else
    echo "You are younger than 18 years old. <br>";

echo "this is the end";


?>
