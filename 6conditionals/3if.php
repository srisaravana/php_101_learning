<?php

$submitted = false;

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php if (!$submitted): ?>
    <div id="form">
        <p>Submit your details</p>
        <div>
            <label>Full name</label>
            <input type="text">
        </div>
        <div>
            <label>Email</label>
            <input type="email">
        </div>
        <div>
            <button type="submit">Submit</button>
        </div>
    </div>
<?php else: ?>

    <div id="results">
        <h3>Thank you for submitting your information</h3>
        <p>We will contact you shortly after.</p>
    </div>

<?php endif; ?>

</body>
</html>
