<?php

$showRed = true;
$showGreen = false;

$name = "david";

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<?php if ($showGreen): ?>
    <h1 style="color: green">My name is <?= $name; ?></h1>
<?php endif; ?>

<?php if ($showRed): ?>
    <h1 style="color: red">My name is <?= $name; ?></h1>
<?php endif; ?>

<?php
if($showGreen){
    echo "<h1 style='color: green'>My name is $name</h1>";
}
?>

</body>
</html>
