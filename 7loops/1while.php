<?php

$index = 0;

while ($index < 10) {
    echo $index . ", ";
    $index++;
}
?>
    <hr>
<?php

$index = 10;
while ($index < 30) {
    echo $index . ", ";
    $index += 2; // index = index + 2
}

?>
    <hr>
<?php

$colors = ["red", "green", "blue"];

$index = 0;
while ($index < count($colors)) {
    echo $colors[$index] . ", ";
    $index++;
}

?>
