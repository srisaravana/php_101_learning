<?php

$index = 0;
$colors = ["red", "green", "blue", "pink", "magenta", "tomato", "lightgreen", "banana"];


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<h3>My favorite colors</h3>
<ul>
    <?php while ($index < count($colors)): ?>
        <li style="color: <?= $colors[$index]; ?>"><?= $colors[$index++]; ?></li>
    <?php endwhile; ?>
</ul>

</body>
</html>
